# SayEye Assets

This is just a companion repository for the SayEye installer build, containing external files needed to build a new installer. If SayEye is to be installed with the cloud server support, keys are to be added to the `application.yml` file before adding it to the installer. Don't commit the keys! Consult the `sayeye-installer` README for more information.

